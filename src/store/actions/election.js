import axios from "axios";
import { createAction } from ".";
import { actionType } from "./type";

export const getElectionData = async (dispatch) => {
  await axios({
    url: "https://api-election.cbsnews.com/api/public/races2/2020/G?filter.officecode=P",
    method: "GET",
  })
    .then((res) => {
      dispatch(createAction(actionType.SET_ELECTION_DATA, res.data));
    })
    .catch((err) => {
      console.log(err);
    });
};
