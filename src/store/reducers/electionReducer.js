import { actionType } from "../actions/type";

const initialState = {
  electionData: [],
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case actionType.SET_ELECTION_DATA:
      state.electionData = action.payload;
      return { ...state };
    default:
      return state;
  }
};
export default reducer;
