import React from "react";
import { useSelector } from "react-redux";
import Header from "../../components/Header/Header";
import StateItem from "../../components/StateItem/StateItem";

export default function State() {
  const electionList = useSelector((state) => state.election.electionData);

  return (
    <div>
      <Header />
      <div className="container text-center">
        <div className=" row">
          {electionList.map((item, index) => (
            <StateItem item={item} key={index} />
          ))}
        </div>
      </div>
    </div>
  );
}
