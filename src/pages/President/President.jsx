import React from "react";
import Header from "../../components/Header/Header";
import avatar_trump from "../../assets/img/trump.png";
import avatar_biden from "../../assets/img/biden.png";
import "./presidentStyle.css";
import { useSelector } from "react-redux";
export default function President() {
  const electionList = useSelector((state) => state.election.electionData);
  console.log(electionList, "electionList");
  const trumpVote =
    electionList
      .map((n) => n.candidates[0])
      .filter((n) => n.fullName === "Donald Trump")
      .map((n) => n.vote)
      .reduce((a, b) => a + b, 0) +
    electionList
      .map((n) => n.candidates[1])
      .filter((n) => n.fullName === "Donald Trump")
      .map((n) => n.vote)
      .reduce((a, b) => a + b, 0);
  const bidenVote =
    electionList
      .map((n) => n.candidates[0])
      .filter((n) => n.fullName === "Joe Biden")
      .map((n) => n.vote)
      .reduce((a, b) => a + b, 0) +
    electionList
      .map((n) => n.candidates[1])
      .filter((n) => n.fullName === "Joe Biden")
      .map((n) => n.vote)
      .reduce((a, b) => a + b, 0);
  const totalVote = trumpVote + bidenVote;
  return (
    <>
      <Header />

      <div className="w-75 mx-auto">
        <div className="title mt-5">
          <h1>President Results</h1>
          <div className="update d-flex justify-content-center">
            <p className="text-danger font-weight-bold mr-3">
              Update at 9M ago
            </p>
            <p>Exit poll +</p>
          </div>
        </div>
        <div className="candidates mt-3">
          <div className="candidate__content d-flex justify-content-center">
            <div className="item d-flex justify-content-around py-3">
              <img
                className="rounded-circle mt-5"
                src={avatar_biden}
                alt="img_biden"
              />
              <div className="info text-white text-right">
                <p className="name font-weight-bold">Joe Biden (D)</p>
                <p className="mb-0">Electoral College</p>
                <p className="score display-3">
                  {electionList
                    .filter((n) => n.rating == "Dem Win")
                    .map((n) => n.electoralVotes)
                    .reduce((a, b) => a + b, 0)}
                </p>
                <p className="text-right">Voted</p>
                <p>
                  <span>{((bidenVote / totalVote) * 100).toFixed(1)}%</span>
                  <span className="ml-4">{bidenVote}</span>
                </p>
              </div>
            </div>
            <div className="item d-flex justify-content-around py-3">
              <div className="info text-left">
                <p className="name font-weight-bold">Donal Trump(R)</p>
                <p className="mb-0">Electoral College</p>
                <p className="score display-3 text-danger ">
                  {electionList
                    .filter((n) => n.rating == "Rep Win")
                    .map((n) => n.electoralVotes)
                    .reduce((a, b) => a + b, 0)}
                </p>
                <p>Voted</p>
                <p className="text-danger">
                  <span>{trumpVote}</span>
                  <span className="mx-4">
                    {((trumpVote / totalVote) * 100).toFixed(1)}%
                  </span>
                </p>
              </div>
              <img
                className="rounded-circle mt-5"
                src={avatar_trump}
                alt="img_trump"
              />
            </div>
          </div>
          <div className="progressbar d-flex justify-content-center mx-auto mt-1">
            <hr className="ratio biden mt-4" />
            <hr className="ratio trump mt-4 " />
          </div>
          <p className="font-weight-bold">270 to Win</p>
        </div>
      </div>
    </>
  );
}
