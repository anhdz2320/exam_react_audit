import React from "react";

export default function Detail() {
  var { id } = useParams();
  useEffect(() => {
    props.getUserData(id);
  }, []);
  return (
    <div className="container">
      {props.userList.userLs
        .filter((x) => x.id == id)
        .map((n, index) => (
          <div className="container detail" key={n.id}>
            <div className="name">{n.stateName}</div>
            <div className="col detail">
              <div className="update row">
                Update: {n.lastUpdated}
                <span className="in ">99% In</span>
              </div>
            </div>
            <div className="col bitru detail">
              <div className="biden row">
                <p> Biden(D): {n.score}</p>
                <p>{n.candidates[0].vote}</p>
                <p>{n.candidates[0].votePct}%</p>
              </div>
              <div className="trump row detail">
                <p> Trump(R): {n.score}</p>
                <p> {n.candidates[1].vote}</p>
                <p> {n.candidates[1].votePct}%</p>
              </div>
            </div>
            <div className="title row detail">
              <p>State Result +</p>
              <p>County Result +</p>
              <p>Exit Poll +</p>
            </div>
          </div>
        ))}
    </div>
  );
}
