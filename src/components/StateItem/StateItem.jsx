import React, { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";

export default function StateItem(props) {
  const [color, setColor] = useState("");
  useEffect(() => {
    if (props.item.candidates[0].fullName.substring(0, 3) === "Don") {
      setColor("red");
    } else if (props.item.candidates[0].fullName.substring(0, 3) === "Joe") {
      setColor("blue");
    }
  }, []);
  const navigate = useNavigate();

  function OpenDetailUser(id) {
    navigate("/detail/" + id);
  }
  return (
    <div className="col-1">
      <button
        className="btn mt-3"
        key={props.item.id}
        onClick={() => OpenDetailUser(props.item.id)}
        style={{ backgroundColor: color }}
      >
        <h5>{props.item.stateCode}</h5>
      </button>
    </div>
  );
}
