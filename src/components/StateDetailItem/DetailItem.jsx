import React, { useEffect, useState } from "react";
import "./DetailItemStyle.css";
export default function DetailItem(props) {
  const [color, setColor] = useState("");
  useEffect(() => {
    if (props.item.candidates[0].fullName.substring(0, 3) === "Don") {
      setColor("red");
    } else if (props.item.candidates[0].fullName.substring(0, 3) === "Joe") {
      setColor("blue");
    }
  }, []);

  return (
    <div className="item col-6">
      <h4 className="font-weight-bold">{props.item.stateName}</h4>
      <p>
        <span className="text-danger">Updated at {props.item.lastUpdated}</span>
        <span>99% in</span>
      </p>
      <div className="col">
        <div className="row resultDetail" style={{ backgroundColor: color }}>
          <p>
            {props.item.candidates[0].fullName}: {props.item.score}
          </p>
          <p>{props.item.candidates[0].vote}</p>
          <p>{props.item.candidates[0].votePct}%</p>
        </div>
        <div className="row resultDetail">
          <p>
            {" "}
            {props.item.candidates[1].fullName}: {props.item.score}
          </p>
          <p> {props.item.candidates[1].vote}</p>
          <p> {props.item.candidates[1].votePct}%</p>
        </div>
      </div>
      <div className="nav">
        <span>State Results+</span>
        <span>Country Results+</span>
        <span>Exit Poll+</span>
      </div>
    </div>
  );
}
